# Version Control

## git - distributed version control
[git-guide](http://rogerdudler.github.io/git-guide/):
nice static web page on git for dummies.

## Try it out
15 min [git tutorial](https://try.github.io/).

Codecademy online [git training](https://www.codecademy.com/learn/learn-git).

## References used for daily work:
Siemens internal [git reference](https://code.siemens.com/help/topics/git/index.md).

Full Tutorial on [Git](https://www.atlassian.com/git/tutorials/what-is-version-control) by Atlassian.

Git [cheat-sheet](https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf).

Pro Git [book](https://git-scm.com/book/en/v2).

Reference [manual](https://git-scm.com/docs).

[Git-Reset-Demystified](https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)
useful readings regarding **working directory**, **staging** and **reset**. 

## git - tooling

* git command line is the best (for beginners and pros alike)
* [SourceTree](https://www.sourcetreeapp.com/), instead of GitGui
* IDE (IntelliJ, Eclipse)
* 3rd party merging tool:
  * [P4Merge](https://www.perforce.com/product/components/perforce-visual-merge-and-diff-tools)
  * [Meld](http://meldmerge.org/)

# Branching strategy

![GitFlow](img/gitflow.png)

[Conventional Changelog](https://wiki.siemens.com/display/en/Conventional+Changelog)

[GitHubFlow](https://guides.github.com/introduction/flow/)

[GitFlow](https://nvie.com/posts/a-successful-git-branching-model/)

[OneFlow](https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow)


# GitLab - SCM
Siemens hosts GitLab Community Edition (CE) at code.siemens.com (accessible from Internet but content requires authentication).
The exact version can be found on [code](https://code.siemens.io).
The vision is to create and share inner source projects.
if you need help regarding Siemens's Gitlab, visit the [help page](https://code.siemens.com/help).
Some fun [stats](https://code.siemens.io/docs/#/statistics).

If you cannot access it you can use [GitLab](https://gitlab.com/) instead.

## Exercise

 * login and generate keypair for authentication
 * create a new project
 * create an issue and/or a merge request
 * do some change (e.g. edit a text file)
 * send merge request for approval

## Markdown

[Gitlab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html)

## gitlab-ci

**Exercise:** create a file `.gitlab-ci.yml` and create a CI for your project.

### Runner
[Gitlab Runner](TODO) in general.

**Demo:** how to set runners.

### Registry
"With the Docker Container Registry integrated into GitLab, every project can
have its own space to store its Docker images."
On [GitLab Docker Registry](https://docs.gitlab.com/ee/user/packages/container_registry/) in general.

**Demo:** how to use image repositories.
